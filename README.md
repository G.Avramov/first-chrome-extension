Test Demo Chrome Extension

This extension adds a custom background to Google.com

Here is how the extension looks like when running:

![Screenshot_2](/uploads/b8ba56ee2dfb50dbef5ef3f5176d288c/Screenshot_2.png)

![Screenshot_3](/uploads/bee6667bd4842bc92e7792726ee3e48e/Screenshot_3.png)
